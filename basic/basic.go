package main

import (
	iplugin "bitbucket.org/goautohome/go-autohome-interface/plugin"
)

type myImpl struct {
}

func (s myImpl)SetConfig(config []byte) string  {
	return ""
}
func (s myImpl)GetInfo() []byte{
	return []byte("{\"Version\":\"0.0.1\",\"EngineName\":\"basic\",\"Type\":\"test\"}")
}
func (s myImpl)Init(dev_json []byte) string{
	return ""
}
func (s myImpl)OnAdd(onAdd func([]byte)){
}

func (s myImpl)OnRemove(onRemove func([]byte)){
}
func (s myImpl)OnUpdate(onUpdate func([]byte, []byte)){
}
func (s myImpl)SetState(dev []byte, fld[]byte) string{
	return ""
}
func (s myImpl) Close(){
}


func GetAutohomePlugin()iplugin.IAutoHomeSimplifiedPlugin{
	return myImpl{}
}