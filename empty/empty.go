package main

import (
	iplugin "bitbucket.org/goautohome/go-autohome-interface/plugin"
	pb "bitbucket.org/goautohome/go-autohome-proto"
)

type myImpl struct {
}

func (pl myImpl) SetConfig(config string) error {
	return nil
}
func (pl myImpl) GetInfo() (iplugin.PluginInfo) {
	return iplugin.PluginInfo{
		Version:"0.0.1",
		EngineName:"empty",
		Type:"test"}
}
func (pl myImpl) Init(d pb.Devices) error {
	return nil
}
func (pl myImpl) OnAdd(onAdd func(device *pb.Device)) {

}
func (pl myImpl) OnRemove(onRemove func(device *pb.Device)) {

}
func (pl myImpl) OnUpdate(onUpdate func(device *pb.Device, field *pb.TraitField)) {

}
func (pl myImpl) SetState(d *pb.Device, field *pb.TraitField) error {
	return nil
}
func (pl myImpl) Close() {

}

func GetAutohomePlugin()iplugin.IAutoHomeSimplifiedPlugin{
	return iplugin.AutoHomePluginImpl{Plugin:myImpl{}}
}